const checkBodyProperties = (body, expectedProperties) => {
    let hasProperties = true;

    let counter = 0;
    while (counter < expectedProperties.length && hasProperties) {
        let property = expectedProperties[counter]; 
        if(body[property] === undefined) {
            hasProperties = false;
        }
        counter++;
    }

    return hasProperties;
};

module.exports = checkBodyProperties;