const jwt = require('jsonwebtoken');

const generateAccessToken = (user) => {
    return jwt.sign({
        'email': user.email,
        'full_name': user.full_name
    }, process.env.JWT_SECRET_KEY, { expiresIn: `${process.env.JWT_EXPIRING_TIME}ms` });
};

module.exports = generateAccessToken;