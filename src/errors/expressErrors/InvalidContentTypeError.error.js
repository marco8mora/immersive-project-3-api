const InvalidContentTypeError = new Error('Invalid Content-Type in request',
{cause: { code: 400 }});
InvalidContentTypeError.name = 'ClientError';

module.exports = InvalidContentTypeError;