const { Op } = require('sequelize');
const ServiceClient = require('../models/serviceClient.model');

const addServiceClient = async (serviceClientData, t = null) => {
    let serviceClient = await ServiceClient.create(serviceClientData, {transaction: t});

    return serviceClient;
};

const getServiceClientById = async (clientId, serviceId, t = null) => {
    let serviceClient = await ServiceClient.findOne({
        where: {
            [Op.and]: [
                { client_id: clientId },
                { service_id: serviceId }
            ]
        },
        transaction: t
    });

    return serviceClient;
};

const updateServiceClient = async (clientId, serviceId, serviceClientData, t = null) => {
    let serviceClient = await ServiceClient.findOne({
        where: {
            [Op.and]: [
                { client_id: clientId },
                { service_id: serviceId }
            ]
        },
        transaction: t
    });

    if(!serviceClient) {
        return null;
    } else {
        await serviceClient.update(serviceClientData, {transaction: t});
        return serviceClient;
    }
};

const deleteServiceClient = async (clientId, serviceId, t = null) => {
    let serviceClient = await ServiceClient.findOne({
        where: {
            [Op.and]: [
                { client_id: clientId },
                { service_id: serviceId }
            ]
        },
        transaction: t
    });

    if(!serviceClient) {
        return null;
    } else {
        await serviceClient.destroy({transaction: t});
        return serviceClient;
    }
};

module.exports = { addServiceClient, getServiceClientById, updateServiceClient, deleteServiceClient };