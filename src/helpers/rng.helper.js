const generateIBANAccountNumber = (prefix) => {
    return prefix + (Math.random().toFixed(20).substring(2))
};

module.exports = { generateIBANAccountNumber };