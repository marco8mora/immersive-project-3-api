const { Op } = require('sequelize');
const sequelize = require('../config/sequelize.config');
const Transaction = require('../models/transaction.model');

const addTransaction = async (transactionData, t = null) => {
    let transaction = await Transaction.create(transactionData, {transaction: t});

    return transaction;
};

const getTransactionById = async (id, t = null) => {
    let transaction = await Transaction.findByPk(id, {transaction: t});

    return transaction;
};

const getTransactionsByAccountNumber = async (number, limit, dateRange = null, t = null) => {
    let transactions = await Transaction.findAll({
            where: dateRange ? {
                    [Op.and]: {
                        [Op.or]: [
                            { account_debited: number },
                            { account_credited: number }
                        ],
                        
                        created_at: {
                            [Op.gte]: dateRange.from,
                            [Op.lte]: dateRange.to
                        }
                    }
                } : {
                    [Op.or]: [
                        { account_debited: number },
                        { account_credited: number }
                    ]
                },
            order: [['created_at', 'DESC']],
            limit: limit,
            transaction: t
        }
    );

    return transactions;
};

const updateTransaction = async (id, transactionData, t = null) => {
    let transaction = await Transaction.findByPk(id, {transaction: t});

    if(!transaction) {
        return null;
    } else {
        await transaction.update(transactionData, {transaction: t});
        return transaction;
    }
};

const deleteTransaction = async (id, t = null) => {
    let transaction = await Transaction.findByPk(id, {transaction: t});

    if(!transaction) {
        return null;
    } else {
        await transaction.destroy({transaction: t});
        return transaction;
    }
};

module.exports = { addTransaction, getTransactionById, getTransactionsByAccountNumber, 
    updateTransaction, deleteTransaction };