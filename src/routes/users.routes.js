const express = require('express');
const authenticate = require('../middleware/authorize.middleware');
const { addUserWithAccounts } = require('../services/bank.service');
const { getUserByEmail } = require('../services/user.service');
const { checkPassword } = require('../helpers/hash.helper');
const generateAccessToken = require('../helpers/token.helper');
const ServerError = require('../errors/expressErrors/ServerError.error');
const DataFormatError = require('../errors/expressErrors/DataFormatError.error');
const UniqueDataDuplicatedError = require('../errors/expressErrors/UniqueDataDuplicatedError.error');
const IncorrectCredentialsError = require('../errors/expressErrors/IncorrectCredentialsError.error');
const NotFoundError = require('../errors/expressErrors/NotFoundError.error');

let usersRouter = express.Router();
let successfulResponse = {
    data: {}
};

/**
 * @swagger
 * /signup:
 *   post:
 *     summary: Create a user account
 *     description: Create a user account that starts with two accounts, one in CRC and one in USD.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewUser'
 *     responses:
 *       200:
 *         description: User created succesfully, the email and full name is returned.
 *         content:
 *           application/json:
 *            schema:
 *             type: object
 *             properties:
 *               data:
 *                 type: object
 *                 properties:
 *                   email:
 *                     type: string
 *                     description: The email of the new user
 *                     example:
 *                       $ref: '#/components/examples/emailExample'
 *                   full_name:
 *                     type: string
 *                     description: The full name of the new user
 *                     example:
 *                       $ref: '#/components/examples/fullNameExample'
 *       400:
 *         $ref: '#/components/responses/400ClientError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
*/
usersRouter.route('/signup')
    .post( async (req, res, next) => {
        const userData = req.body;
        try {
            const newUserWithAccounts = await addUserWithAccounts(userData);
            successfulResponse.data = {
                email: newUserWithAccounts.email,
                full_name: newUserWithAccounts.full_name
            }
            res.json(successfulResponse);
        } catch (error) {
            if(error.name == 'SequelizeValidationError') {
                next(DataFormatError);
            } else if(error.name == 'SequelizeUniqueConstraintError'){
                next(UniqueDataDuplicatedError(error.fields));
            } else {
                next(ServerError);
            }
        }
    });

let cookieObj = {};
if (process.env.PLATFORM === "HEROKU") {
    cookieObj = {
        sameSite: "none",
        httpOnly: true,
        secure: true,
        maxAge: Number(process.env.JWT_EXPIRING_TIME)
    };
}
else
{
    cookieObj = {
        httpOnly: true,
        secure: false
    };
}

/**
 * @swagger
 * /login:
 *   post:
 *     summary: Log in an existing user to the application
 *     description: Takes the provided credentials an compares them againts the database to find a match. If a match is found, creates and sends back a JSON Web Token.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: The user's email
 *                 example: 
 *                   $ref: '#/components/examples/emailExample'
 *               password:
 *                 type: string
 *                 description: The user's password
 *                 example:
 *                   $ref: '#/components/examples/passwordExample'
 *     responses:
 *       200:
 *         description: User logged in successfully and JWT assigned.
 *         content:
 *           application/json:
 *            schema:
 *             type: object
 *             properties:
 *               data:
 *                 type: string
 *                 description: The JWT assigned for authorization.
 *                 example: xxxxx.yyyyy.zzzzz
 *       400:
 *         $ref: '#/components/responses/400ClientError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
*/
usersRouter.route('/login')
    .post( async (req, res, next) => {
        const loginCredentials = req.body;
        try {
            const user = await getUserByEmail(loginCredentials.email);
            if(!user){
                next(IncorrectCredentialsError);
            } else {
                const passwordMatch = await checkPassword(loginCredentials.password, 
                    user.password);
                if(passwordMatch) {
                    successfulResponse.data = generateAccessToken(user)
                    res.json(successfulResponse);
                } else {
                    next(IncorrectCredentialsError);
                }
            }

        } catch (error) {
            next(ServerError);
        }
    });

usersRouter.route('/verify')
    .get(authenticate, async (req, res, next) => {
        successfulResponse.data = {};
        res.json(successfulResponse);
    });

/**
 * @swagger
 * /users:
 *   get:
 *     summary: Retrieve a single user.
 *     description: Retrieve a single user, can be use to get a user's profile information.
 *     responses:
 *       200:
 *         description: User retrieved successfully.
 *         content:
 *           application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *       404:
 *         $ref: '#/components/responses/404NotFoundError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 *       
 */
usersRouter.route('/user')
    .get (authenticate, async (req, res, next) => {
        try {
            const user = await getUserByEmail(req.jwt_payload.email);
            if(!user) {
                next(NotFoundError({email: req.email}));
            } else {
                const { password, deleted_at, ...rest } = user.toJSON();
                successfulResponse.data = rest;
                res.json(successfulResponse);
            }
        } catch (error) {
            next(ServerError);
        }
    });

module.exports = usersRouter;