const ExchangeNotAvailableError = new Error('Currency exchange not available');
ExchangeNotAvailableError.name = 'ExchangeError';

module.exports = ExchangeNotAvailableError;