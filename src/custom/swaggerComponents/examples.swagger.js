/**
 * @swagger
 * components:
 *   examples:
 *     emailExample: example@example.com
 *     fullNameExample: John Doe
 *     passwordExample: password1234
 *     accountNumberExample: CR00000000000000000000
 *     transactionIdExample: 582dc9f6-fa57-472b-b485-c3dd98df29d4
 *     datetimeExample: 2022-06-30T15:17:42.951Z
 *     dateExample: 2022-06-30
*/