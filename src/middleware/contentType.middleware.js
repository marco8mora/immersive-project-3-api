const InvalidContentTypeError = require('../errors/expressErrors/InvalidContentTypeError.error')

const contentTypeCheck = (req, res, next) => {
    let contentType = req.get('Content-Type');
    if(contentType !== 'application/json') {
        next(InvalidContentTypeError);
    } else {
        next();
    }
}

module.exports = contentTypeCheck;