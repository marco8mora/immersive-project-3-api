const express = require('express');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const contentTypeCheck = require('./middleware/contentType.middleware');
const error = require('./middleware/error.middleware');
const usersRouter = require('./routes/users.routes');
const accountsRouter = require('./routes/accounts.routes');
const servicesRouter = require('./routes/services.routes');
const transactionsRouter = require('./routes/transactions.routes');
const exchangesRouter = require('./routes/exchanges.routes');
require('./models/associations');

const app = express();

//--------SWAGGER SETUP---------

const swaggerDefinition = {
    openapi: '3.0.3',
    info: {
      title: `Immersive Project 3 API`,
      version: '1.2.0',
      description: `This is the API application for the third project at Konrad's Immersive Program`,
      license: {
        name: 'Licensed Under MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
      contact: {
        name: 'Marco Mora',
        url: 'https://gitlab.com/marco8mora/immersive-project-3-api',
        email: 'marco8mora@gmail.com'
      }
    }
};

const options = {
    swaggerDefinition,
    apis: ['src/routes/*.js', 'src/custom/swaggerComponents/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

//--------MIDDLEWARE SETUP---------

console.log("App platform: ", process.env.PLATFORM);
const corsObj = {
    origin: process.env.ORIGIN,
    credentials: true
};

app.use(cors(corsObj));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());

app
    .post('/*', contentTypeCheck)
    .put('/*', contentTypeCheck);

//---------ROUTER SETUP---------

app.use(usersRouter);
app.use('/accounts', accountsRouter);
app.use('/services', servicesRouter);
app.use('/transactions', transactionsRouter);
app.use('/exchange', exchangesRouter);

//---------ERROR HANDLER---------

app.use(error);

module.exports = app;