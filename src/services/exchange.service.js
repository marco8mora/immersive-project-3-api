const { Op } = require('sequelize');
const Exchange = require('../models/exchange.model');

const addExchange = async (exchangeData, t = null) => {
    let exchange = await Exchange.create(exchangeData, {transaction: t});

    return exchange;
};

const getExchangeByCurrencies = async (from, to, t = null) => {
    let exchange = await Exchange.findOne({
        where: {
            [Op.and]: [
                { from: from },
                { to: to }
            ]
        },
        transaction: t});

    return exchange;
};

const updateExchange = async (from, to, exchangeData, t = null) => {
    let exchange = await Exchange.findOne({
        where: {
            [Op.and]: [
                { from: from },
                { to: to }
            ]
        },
        transaction: t});

    if(!exchange) {
        return null;
    } else {
        await exchange.update(exchangeData, {transaction: t});
        return exchange;
    }
};

const deleteExchange = async (from, to, t = null) => {
    let exchange = await Exchange.findOne({
        where: {
            [Op.and]: [
                { from: from },
                { to: to }
            ]
        },
        transaction: t});

    if(!exchange) {
        return null;
    } else {
        await exchange.destroy({transaction: t});
        return exchange;
    }
};

const exchangeCurrencies = async (amount, from, to, t = null) => {
    let exchange = await Exchange.findOne({
        where: {
            [Op.and]: [
                { from: from },
                { to: to }
            ]
        },
        transaction: t});

    if (!exchange) {
        return null;
    } else {
        return amount * exchange.value;
    }
};

module.exports = { addExchange, getExchangeByCurrencies, 
    updateExchange, deleteExchange, exchangeCurrencies };