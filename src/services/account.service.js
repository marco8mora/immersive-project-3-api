const Account = require('../models/account.model');

const addAccount = async (accountData, t = null) => {
    let account = await Account.create(accountData, {transaction: t});

    return account;
};

const getAccountByNumber = async (number, t = null) => {
    let account = await Account.findByPk(number, {transaction: t});

    return account;
};

const getAccountsByUserEmail = async (email, t = null) => {
    let accounts = await Account.findAll({where: {user_email: email}, transaction: t});

    if (!accounts.length) {
        return null
    } else {
        return accounts;
    }
};

const updateAccount = async (number, accountData, t = null) => {
    let account = await Account.findByPk(number, {transaction: t});

    if(!account) {
        return null;
    } else {
        await account.update(accountData, {transaction: t});
        return account;
    }
};

const deleteAccount = async (number, t = null) => {
    let account = await Account.findByPk(number, {transaction: t});

    if(!account) {
        return null;
    } else {
        await account.destroy({transaction: t});
        return account;
    }
};

module.exports = { addAccount, getAccountByNumber, getAccountsByUserEmail, 
    updateAccount, deleteAccount };