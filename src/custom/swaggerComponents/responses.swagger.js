/**
 * @swagger
 * components:
 *   responses:
 *     400ClientError:
 *       description: Error ocurred due to client's request. The data object can contain the erroneous data.
 *       content:
 *         application/json:
 *           schema:
 *             anyOf:
 *               - $ref: '#/components/schemas/Error'
 *               - $ref: '#/components/schemas/ErrorWithData'
 *     401NotAuthorizedError:
 *       description: The user is not authorized to access the resource.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Error'
 *     404NotFoundError:
 *       description: The resource requested was not found. The data object contains the parameter received.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ErrorWithData'
 *     409ConflictError:
 *       description: A transaction could not be completed due to the current state of the application. The data object contains the cause of the error.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ErrorWithData'
 *     500ServerError:
 *       description: Error due to an internal process of the application.
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Error'
*/