const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const RecurrentService = sequelize.define('RecurrentService', {
    date_paid: {
        type: DataTypes.DATEONLY,
        allowNull: false,
        validate: {
            isDate: true
        }
    },
    client_id: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [1,254]
        }
    }
}, {
    tableName: 'recurrent_service',
    timestamps: false
});

module.exports = RecurrentService;