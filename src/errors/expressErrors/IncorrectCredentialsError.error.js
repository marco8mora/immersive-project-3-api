const IncorrectCredentialsError = new Error('Incorrect log in credentials', 
{cause: { code: 400}});
IncorrectCredentialsError.name = 'ClientError';

module.exports = IncorrectCredentialsError;