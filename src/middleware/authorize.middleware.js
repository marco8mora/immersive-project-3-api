const jwt = require('jsonwebtoken');
const UserNotAuthorizedError = require('../errors/expressErrors/UserNotAuthorizedError.error');
const InvalidTokenError = require('../errors/expressErrors/InvalidTokenError.error');

function authenticate(req, res, next) {
  const token = req.cookies.auth_token;
  
  if(!token){
    next(UserNotAuthorizedError);
  } else {
    try {
      jwt.verify(token, process.env.JWT_SECRET_KEY);
      let decoded = jwt.decode(token, {complete: true});
      req.jwt_payload = decoded.payload;
      next();
    } catch (err) {
      next(InvalidTokenError);
    }
  }

}

module.exports = authenticate;