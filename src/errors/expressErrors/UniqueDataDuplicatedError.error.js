const UniqueDataDuplicatedError = (data) => {
    let error = new Error('Unique values already registered', { cause: { code: 400, data: data }})
    error.name = 'UniqueDataError';
    return error;
};

module.exports = UniqueDataDuplicatedError;