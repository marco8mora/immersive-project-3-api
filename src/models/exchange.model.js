const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const Exchange = sequelize.define('Exchange', {
    from: {
        type: DataTypes.STRING,
        primaryKey: true,
        validate: {
            is: {
                args: /[A-Z]{3}/,
                msg: 'Not a valid currency code'
            },
            len: [3,3]
        }
    },
    to: {
        type: DataTypes.STRING,
        primaryKey: true,
        validate: {
            is: {
                args: /[A-Z]{3}/,
                msg: 'Not a valid currency code'
            },
            len: [3,3]
        }
    },
    value: {
        type: DataTypes.REAL,
        allowNull: false,
        validate: {
            isFloat: true
        }
    }
}, {
    tableName: 'exchange',
    timestamps: false
});

module.exports = Exchange;