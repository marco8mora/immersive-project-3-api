const User = require('../models/user.model');

const addUser = async (userData, t = null) => {
    let user = await User.create(userData, {transaction: t});

    return user;
};

const getUserByEmail = async (email, t = null) => {
    let user = await User.findByPk(email, {transaction: t});

    return user;
};

const updateUser = async (email, userData, t = null) => {
    let user = await User.findByPk(email, {transaction: t});

    if(!user) {
        return null;
    } else {
        await user.update(userData, {transaction: t});
        return user;
    }
};

const deleteUser = async (email, t = null) => {
    let user = await User.findByPk(email, {transaction: t});

    if(!user) {
        return null;
    } else {
        await user.destroy({transaction: t});
        return user;
    }
};

module.exports = { addUser, getUserByEmail, updateUser, deleteUser };