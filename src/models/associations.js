const { DataTypes } = require('sequelize');
const User = require('./user.model');
const Account = require('./account.model');
const Transaction = require('./transaction.model');
const Service = require('./service.model');
const ServiceClient = require('./serviceClient.model');
const RecurrentService = require('./recurrentService.model');

// User - Account 1:N

Account.belongsTo(User, {
    foreignKey: {
        name: 'user_email',
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
            len: [1,254],
            isEmail: true
        }
    },
    onUpdate: 'CASCADE',
    onDelete: 'RESTRICT'
});

// Account - Account M:N -> Transaction

Account.belongsToMany(Account, 
    { 
        as: 'account_debited',
        through: Transaction, 
        foreignKey: {
            name: 'account_debited',
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: [22,22]
            }
        }, 
        uniqueKey: false,
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
    });

Account.belongsToMany(Account, 
    {
        as: 'account_credited', 
        through: Transaction, 
        foreignKey: {
            name: 'account_credited',
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [22,22]
            }
        },
        uniqueKey: false,
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
    });

// User - Service M:N -> RecurrentService

User.belongsToMany(Service, 
    { 
        through: RecurrentService, 
        foreignKey: 'user_email', 
        uniqueKey: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    });

Service.belongsToMany(User, 
    { 
        through: RecurrentService, 
        foreignKey: 'service_id', 
        uniqueKey: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    });

// Service - Account 1:1

Service.belongsTo(Account, {
    foreignKey: {
        name: 'account_number',
        type: DataTypes.STRING,
        validate: {
            len: [22,22]
        }
    }
});

// Service - ServiceClient 1:N

ServiceClient.belongsTo(Service, {
    foreignKey: {
        name: 'service_id',
        type: DataTypes.INTEGER,
        primaryKey: true,
        validate: {
            isInt: true
        }
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'
});