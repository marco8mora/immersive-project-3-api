const error = (err, req, res, next) => {
    res.statusCode = err.cause.code;
    res.json({
        error: {
            name: err.name,
            message: err.message
        },
        data: err.cause.data ? err.cause.data : {}
    });
}

module.exports = error;