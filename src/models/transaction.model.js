const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const Transaction = sequelize.define('Transaction', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
        validate: {
            isUUID: 4
        }
    },
    amount: {
        type: DataTypes.REAL,
        allowNull: false,
        validate: {
            isFloat: true
        }
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [1, 100]
        }
    },
    is_service_payment: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 'false'
    },
    external_account: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
            len: [22,22]
        }
    }
}, {
    sequelize,
    tableName: 'transaction',
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: false,
    deletedAt: 'deleted_at'
});

module.exports = Transaction;