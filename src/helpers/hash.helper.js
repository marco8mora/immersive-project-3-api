const bcrypt = require('bcrypt');

const hashPassword = (password) => {
    let hashedPassword = bcrypt.hashSync(password, Number(process.env.SALT_ROUNDS));

    return hashedPassword;
}

const checkPassword = async (password, hash) => {
    let comparisonResult = await bcrypt.compare(password, hash)
        .then( (result) => {return result});

    return comparisonResult;
}

module.exports = {hashPassword, checkPassword};