const NotFoundError = (data) => {
    let error = new Error('Not found', { cause: { code: 404, data: data }})
    error.name = 'ClientError';
    return error;
};

module.exports = NotFoundError;