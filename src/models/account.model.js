const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const currencies = [
    'crc',
    'usd'
];

const Account = sequelize.define('Account', {
    number: {
        type: DataTypes.STRING,
        primaryKey: true,
        validate: {
            is: {
                args: /^[A-Z]{2}[0-9]{20}$/,
                msg: 'The account number does not correspond to a valid IBAN number'
            },
            len: [22,22],
        }
    },
    currency: {
        type: DataTypes.ENUM,
        values: currencies,
        allowNull: false,
        validate: {
            isIn: [currencies]
        }
    },
    balance: {
        type: DataTypes.REAL,
        allowNull: false,
        validate: {
            isFloat: true
        }
    }
}, {
    sequelize,
    tableName: 'account',
    paranoid: true,
    createdAt: false,
    updatedAt: false,
    deletedAt: 'deleted_at'
});

module.exports = Account;