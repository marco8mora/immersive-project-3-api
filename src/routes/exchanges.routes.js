const express = require('express');
const { exchangeCurrencies } = require('../services/exchange.service');
const checkBodyProperties = require('../helpers/bodyChecker.helper');
const ServerError = require('../errors/expressErrors/ServerError.error');
const TransactionError = require('../errors/expressErrors/TransactionError.error');
const DataFormatError = require('../errors/expressErrors/DataFormatError.error');

let exchangesRouter = express.Router();
let successfulResponse = {
    data: {}
};

const expectedExchangeProperties = [
    'from',
    'to',
    'amount'
];

/**
 * @swagger
 * /exchange:
 *   post:
 *     summary: Exchange one currency to another.
 *     description: Exchange one currency to another. The exchange may not be available in the bank, in that case an error is returned.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               from:
 *                 type: string
 *                 description: The origin currency of the exchange.
 *                 enum: [crc, usd]
 *                 example: usd
 *               to:
 *                 type: string
 *                 description: The destination currency of the exchange.
 *                 enum: [crc, usd]
 *                 example: crc
 *               amount:
 *                 type: number
 *                 description: The amount to exchange.
 *                 example: 150.05
 *     responses:
 *       200:
 *         description: Exchange performed successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: number
 *                   description: The amount exchanged.
 *                   example: 200000.35
 *       400:
 *         $ref: '#/components/responses/400ClientError'
 *       409:
 *         $ref: '#/components/responses/409ConflictError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 */
exchangesRouter.route('/')
    .post( async (req, res, next) => {
        const exchangeData = req.body;
        if (!checkBodyProperties(exchangeData, expectedExchangeProperties)) {
            next(DataFormatError);
        } else if (isNaN(Number(exchangeData.amount))) {
            next(DataFormatError);
        } else {
            try {
                const exchange = await exchangeCurrencies(exchangeData.amount,
                    exchangeData.from, exchangeData.to);
                if(!exchange) {
                    next(TransactionError('ExchangeNotAvailable'))
                } else {
                    successfulResponse.data = parseFloat((exchange).toFixed(2));
                    res.json(successfulResponse);
                }
            } catch (error) {
                next(ServerError);
            }
        }
    })

module.exports = exchangesRouter;