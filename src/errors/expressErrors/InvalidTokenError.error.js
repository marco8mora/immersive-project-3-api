const InvalidTokenError = new Error('Invalid or expired authorization token', 
{cause: { code: 401}});
InvalidTokenError.name = 'InvalidTokenError';

module.exports = InvalidTokenError;