const { Sequelize } = require('sequelize');

const DATABASE_URL = process.env.DATABASE_URL;

let sequelize = null;

try {
    sequelize = new Sequelize(DATABASE_URL, {
        dialect: 'postgres'
    });
} catch(error) {
    console.error('Unable to connect to the database');
    throw error;  
}

module.exports = sequelize;