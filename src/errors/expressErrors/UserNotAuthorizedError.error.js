const UserNotAuthorizedError = new Error('User not allowed to access this resource', 
{cause: { code: 401}});
UserNotAuthorizedError.name = 'NotAuthorizedError';

module.exports = UserNotAuthorizedError;