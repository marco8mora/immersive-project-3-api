const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const ServiceClient = sequelize.define('ServiceClient', {
    client_id: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
        validate: {
            len: [1,254]
        }
    },
    amount_owed: {
        type: DataTypes.REAL,
        allowNull: false,
        validate: {
            isFloat: true
        }
    }
}, {
    tableName: 'service_client',
    timestamps: false
});

module.exports = ServiceClient;