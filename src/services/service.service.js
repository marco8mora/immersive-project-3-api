const Service = require('../models/service.model');

const addService = async (serviceData, t = null) => {
    let service = await Service.create(serviceData, {transaction: t});

    return service;
};

const getAllServices = async (t = null) => {
    let services = await Service.findAll({transaction: t});

    return services
};

const getServiceById = async (id, t = null) => {
    let service = await Service.findByPk(id, {transaction: t});

    return service;
};

const updateService = async (id, serviceData, t = null) => {
    let service = await Service.findByPk(id, {transaction: t});

    if(!service) {
        return null;
    } else {
        await service.update(serviceData, {transaction: t});
        return service;
    }
};

const deleteService = async (id, t = null) => {
    let service = await Service.findByPk(id, {transaction: t});

    if(!service) {
        return null;
    } else {
        await service.destroy({transaction: t});
        return service;
    }
};

module.exports = { addService, getAllServices, getServiceById, updateService, deleteService };