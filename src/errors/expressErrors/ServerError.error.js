const ServerError = new Error('API could not process the request', 
{cause: { code: 500}});
ServerError.name = 'ServerError';

module.exports = ServerError;