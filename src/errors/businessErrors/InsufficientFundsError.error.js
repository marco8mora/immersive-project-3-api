const InsufficientFundsError = new Error('Insufficient funds in account');
InsufficientFundsError.name = 'InsufficientFundsError';

module.exports = InsufficientFundsError;