const express = require('express');
const authenticate = require('../middleware/authorize.middleware');
const { getAccountByNumber } = require('../services/account.service');
const { getTransactionsByAccountNumber } = require('../services/transaction.service');
const { addMoney, transferMoney } = require('../services/bank.service');
const checkBodyProperties = require('../helpers/bodyChecker.helper');
const DataFormatError = require('../errors/expressErrors/DataFormatError.error');
const ServerError = require('../errors/expressErrors/ServerError.error');
const NotFoundError = require('../errors/expressErrors/NotFoundError.error');
const UserNotAuthorizedError = require('../errors/expressErrors/UserNotAuthorizedError.error');
const TransactionError = require('../errors/expressErrors/TransactionError.error');

let transactionsRouter = express.Router();
let successfulResponse = {
    data: {}
};

transactionsRouter.param('account_number', (req, res, next, account_number) => {
    req.account_number = account_number;
    next();
});

/**
 * @swagger
 * /transactions/{account_number}:
 *   get:
 *     summary: Retrieve an account's transactions.
 *     description: Retrieve all transactions registered to an account, credit or debit.
 *     parameters:
 *       - in: path
 *         name: account_number
 *         required: true
 *         description: The account number to retrieve.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Transactions retrieved successfully.
 *         content:
 *           application/json:
 *            schema:
 *              $ref: '#/components/schemas/FullTransaction'
 *       401:
 *         $ref: '#/components/responses/401NotAuthorizedError'
 *       404:
 *         $ref: '#/components/responses/404NotFoundError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'   
 */
transactionsRouter.route('/:account_number')
    .get(authenticate, async (req, res, next) => {
        try {
            const account = await getAccountByNumber(req.account_number);
            if (!account) {
                // Check if account exists
                next(NotFoundError({accountNumber: req.account_number}))
            } else if (!account.user_email) {
                // Check if the account consulted is a services's account
                next(UserNotAuthorizedError);
            } else {
                const user = await account.getUser();
                if(user.email !== req.jwt_payload.email) {
                    // Check if the user logged in is the owner of the account
                    next(UserNotAuthorizedError);
                } else {
                    const limit = req.query.limit;
                    const dateRange = (req.query.from && req.query.to) ? 
                        {
                            from: new Date(req.query.from).setHours(0,0,0,0), 
                            to: new Date(req.query.to).setHours(23,59,59,999) 
                        } : null;
                    const transactions = await getTransactionsByAccountNumber(req.account_number,
                        limit, dateRange);
                    successfulResponse.data = transactions.map(transaction => {
                        const {deleted_at, ...rest} = transaction.toJSON();
                        return rest;
                    });
                    res.json(successfulResponse);
                }
            }
        } catch (error) {
            next(ServerError);
        }
    });

const expectedAddMoneyProperties = [
    'account_credited',
    'amount',
    'description',
    'external_account'
]

/**
 * @swagger
 * /transactions/addmoney:
 *   post:
 *     summary: Add funds to account.
 *     description: Add funds to an account in the bank from an external account.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               account_credited:
 *                 type: string
 *                 description: The destination account for the funds. The user must be the owner of the account.
 *                 example: 
 *                   $ref: '#/components/examples/accountNumberExample'
 *               amount:
 *                 type: number
 *                 description: The amount transfered in the destinations account currency.
 *                 example: 245.00
 *               description:
 *                 type: string
 *                 description: A description of the transaction.
 *                 example: Payment for services provided.
 *               external_account:
 *                 type: string
 *                 description: The account from another bank where the funds are coming from.
 *                 example:
 *                   $ref: '#/components/examples/accountNumberExample'
 *     responses:
 *       200:
 *         description: Funds added succesfully.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ExternalTransaction'
 *       400:
 *         $ref: '#/components/responses/400ClientError'
 *       401:
 *         $ref: '#/components/responses/401NotAuthorizedError'
 *       404:
 *         $ref: '#/components/responses/404NotFoundError'
 *       409:
 *         $ref: '#/components/responses/409ConflictError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 */
transactionsRouter.route('/addmoney')
    .post(authenticate, async (req, res, next) => {
        const transactionData = req.body;
        if (!checkBodyProperties(transactionData, expectedAddMoneyProperties)) {
            next(DataFormatError);
        } else {
            try {
                const account = await getAccountByNumber(transactionData.account_credited);
                if (!account) {
                    next(NotFoundError({account_credited: transactionData.account_credited}));
                } else if (!account.user_email) {
                    next(UserNotAuthorizedError);
                } else {
                    const user = await account.getUser();
                    if(user.email !== req.jwt_payload.email) {
                        // Check if the user logged in is the owner of the credited account
                        next(UserNotAuthorizedError);
                    } else {
                        const transaction = await addMoney(transactionData);
                        const { deleted_at, account_debited, is_service_payment, ...rest } = transaction.toJSON();
                        successfulResponse.data = rest;
                        res.json(successfulResponse);
                    } 
                }
            } catch (error) {
                if (error.name === 'SequelizeValidationError') {
                    next(DataFormatError);
                } else {
                    next(ServerError);
                }
            }
        }
    });

const expectedTransferProperties = [
    'account_credited',
    'account_debited',
    'amount',
    'description',
]

/**
 * @swagger
 * /transactions/transfer:
 *   post:
 *     summary: Transfer funds between accounts
 *     description: Transfer funds between accounts in the bank. If the currency of the accounts id different, the conversion is done automatically if the exchange coversion is available.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               account_credited:
 *                 type: string
 *                 description: The destination account for the funds.
 *                 example: 
 *                   $ref: '#/components/examples/accountNumberExample'
 *               account_debited:
 *                 type: string
 *                 description: The origin account of the funds. The user must be the owner of the account.
 *                 example: 
 *                   $ref: '#/components/examples/accountNumberExample'
 *               amount:
 *                 type: number
 *                 description: The amount transfered in the destinations account currency.
 *                 example: 245.00
 *               description:
 *                 type: string
 *                 description: A description of the transaction.
 *                 example: Payment for services provided.
 *     responses:
 *       200:
 *         description: Funds transfered succesfully.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/InternalTransaction'
 *       400:
 *         $ref: '#/components/responses/400ClientError'
 *       401:
 *         $ref: '#/components/responses/401NotAuthorizedError'
 *       404:
 *         $ref: '#/components/responses/404NotFoundError'
 *       409:
 *         $ref: '#/components/responses/409ConflictError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 */
transactionsRouter.route('/transfer')
    .post(authenticate, async (req, res, next) => {
        const transactionData = req.body;
        if (!checkBodyProperties(transactionData, expectedTransferProperties)) {
            next(DataFormatError);
        } else {
            try {
                const account = await getAccountByNumber(transactionData.account_debited);
                if (!account) {
                    next(NotFoundError({account_debited: transactionData.account_debited}));
                } else if (!account.user_email) {
                    next(UserNotAuthorizedError);
                } else {
                    const user = await account.getUser();
                    if(user.email !== req.jwt_payload.email) {
                        // Check if the user logged in is the owner of the debited account
                        next(UserNotAuthorizedError);
                    } else {
                        const transaction = await transferMoney(transactionData);
                        const { deleted_at, is_service_payment, external_account, ...rest } = transaction.toJSON();
                        successfulResponse.data = rest;
                        res.json(successfulResponse);
                    }
                }
            } catch (error) {
                if(error.name === 'ItemNotFoundError') {
                    next(NotFoundError({account_credited: error.cause.data}))
                } else if (error.name === 'InsufficientFundsError') {
                    next(TransactionError('InsufficientFunds'));
                } else if (error.name === 'ExchangeError') {
                    next(TransactionError('ExchangeNotAvailable'));
                } else if(error.name === 'SequelizeValidationError') {
                    next(DataFormatError);
                } else {
                    next(ServerError);
                }
            }
        }
    });

module.exports = transactionsRouter;