const express = require('express');
const authenticate = require('../middleware/authorize.middleware');
const { getAllServices, getServiceById } = require('../services/service.service');
const { addServiceWithAccount, payService } = require('../services/bank.service');
const { getUsersRecurrentServicesByEmail } = require('../services/recurrentService.service');
const { getAccountByNumber } = require('../services/account.service');
const checkBodyProperties = require('../helpers/bodyChecker.helper');
const ServerError = require('../errors/expressErrors/ServerError.error');
const DataFormatError = require('../errors/expressErrors/DataFormatError.error');
const NotFoundError = require('../errors/expressErrors/NotFoundError.error');
const UserNotAuthorizedError = require('../errors/expressErrors/UserNotAuthorizedError.error');
const UniqueDataDuplicatedError = require('../errors/expressErrors/UniqueDataDuplicatedError.error');
const TransactionError = require('../errors/expressErrors/TransactionError.error');

let servicesRouter = express.Router();
let successfulResponse = {
    data: {}
};

/**
 * @swagger
 * /services:
 *   get:
 *     summary: Retrieve all services.
 *     description: Retrieve all availables services.
 *     responses:
 *       200:
 *         description: All services retrieved successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Service'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 *   post:
 *     summary: Add a new service
 *     description: Adds a new service and creates an account for that service in CRC
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewService'
 *     responses:
 *       200:
 *         description: Service added succesfully with a CRC account.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Service'
 *       400:
 *         $ref: '#/components/responses/400ClientError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 */
servicesRouter.route('/')
    .get( async (req, res, next) => {
        try {
            const services = await getAllServices();
            successfulResponse.data = services.map(service => service.toJSON());
            res.json(successfulResponse);
        } catch (error) {
            next(ServerError);
        }
    })
    .post(authenticate, async (req, res, next) => {
        const serviceData = req.body;
        try {
            const service = await addServiceWithAccount(serviceData);
            successfulResponse.data = service;
            res.json(service);
        } catch (error) {
            if(error.name == 'SequelizeValidationError') {
                next(DataFormatError);
            }  else if (error.name == 'SequelizeUniqueConstraintError') {
                next(UniqueDataDuplicatedError(error.fields));
            } else {
                next(ServerError);
            }
        }
    });

const expectedPaymentProperties = [
    'client_id', 
    'service_id', 
    'account_debited',
    'set_as_recurrent'
];

/**
 * @swagger
 * /services/pay:
 *   post:
 *     summary: Pay an owed service.
 *     description: Checks if the queried client owes something to the service and transfer the amount from the user's account to the service's account.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               client_id:
 *                 type: string
 *                 description: Service's client information to associated with a bill.
 *                 example: 88888888
 *               service_id:
 *                 type: string
 *                 description: Service's ID.
 *                 example: 16
 *               account_debited:
 *                 type: string
 *                 description: Account from where the funds will bew substracted.
 *                 example: 
 *                   $ref: '#/components/examples/accountNumberExample'
 *               set_as_recurrent:
 *                 type: boolean
 *                 description: Indicator to add service to recurrent payments.
 *                 example: true
 *     responses:
 *       200:
 *         description: Service paid successfully. Returns the transaction. If data is empty, the client queried didn't owed anything to the service.
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                 - $ref: '#/components/schemas/ServiceTransaction'
 *                 - type: object
 *                   description: Only empty when the queried client didn't owed anything to the service.
 *                   example: {}
 *       400:
 *         $ref: '#/components/responses/400ClientError'
 *       401:
 *         $ref: '#/components/responses/401NotAuthorizedError'
 *       404:
 *         $ref: '#/components/responses/404NotFoundError'
 *       409:
 *         $ref: '#/components/responses/409ConflictError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 */
servicesRouter.route('/pay')
    .post(authenticate, async (req, res, next) => {
        const paymentData = req.body;
        if (!checkBodyProperties(paymentData, expectedPaymentProperties)) {
            next(DataFormatError);
        } else {
            try {
                const account = await getAccountByNumber(paymentData.account_debited);
                if (!account) {
                    next(NotFoundError({account_debited: paymentData.account_debited}));
                } else if (!account.user_email) {
                    next(UserNotAuthorizedError);
                } else {
                    const user = await account.getUser();
                    if(user.email !== req.jwt_payload.email) {
                        // Check if the user logged in is the owner of the account
                        next(UserNotAuthorizedError);
                    } else {
                        paymentData.email = user.email;
                        const transaction = await payService(paymentData)
                        const { is_service_payment, deleted_at, external_account, ...rest } = transaction.toJSON();
                        successfulResponse.data = rest;
                        res.json(successfulResponse);
                    }
                }
            } catch (error) {
                if(error.name === 'ItemNotFoundError' && error.cause.item === 'bill_to_pay') {
                    successfulResponse.data = {};
                    res.json(successfulResponse);
                } else if (error.name === 'InsufficientFundsError') {
                    next(TransactionError('InsufficientFunds'));
                } else if (error.name === 'ExchangeError') {
                    next(TransactionError('ExchangeNotAvailable'));
                } else {
                    next(ServerError);
                }
            }
        }
    });

/**
 * @swagger
 * /services/recurrent:
 *   get:
 *     summary: Retrieve an user's recurrent services.
 *     description: Retrieve an user's recurrent services, can be use to display the recurrent services to the user.
 *     responses:
 *       200:
 *         description: Recurrent services retrieved successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Service'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 */
servicesRouter.route('/recurrent')
    .get(authenticate, async (req, res, next) => {
        try {
            const recurrentServices = await getUsersRecurrentServicesByEmail(req.jwt_payload.email);
            successfulResponse.data = recurrentServices.map(service => service.toJSON());
            res.json(successfulResponse);
        } catch (error) {
            next(ServerError);
        }
    });

servicesRouter.param('id', (req, res, next, id) => {
    req.id = id;
    next();
});

/**
 * @swagger
 * /services/{id}:
 *   get:
 *     summary: Retrieve a single service.
 *     description: Retrieve a single user by ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: The ID associated with the service.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Service retrieved successfully.
 *         content:
 *           application/json:
 *            schema:
 *              $ref: '#/components/schemas/Service'
 *       404:
 *         $ref: '#/components/responses/404NotFoundError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'   
 */
servicesRouter.route('/:id')
    .get(async (req, res, next) => {
        try {
            const service = await getServiceById(req.id);
            if(!service) {
                next(NotFoundError({id: req.id}));
            } else {
                successfulResponse.data = service.toJSON();
                res.json(successfulResponse);
            }
        } catch (error) {
            next(ServerError);
        }
    });


module.exports = servicesRouter;