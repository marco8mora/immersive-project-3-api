const { Op } = require('sequelize');
const RecurrentService = require('../models/recurrentService.model');

const addRecurrentService = async (recurrentServiceData, t = null) => {
    let recurrentService = await RecurrentService.create(recurrentServiceData, {transaction: t});

    return recurrentService;
};

const getRecurrentServiceById = async (userEmail, serviceId, t = null) => {
    let recurrentService = await RecurrentService.findOne({
        where: {
            [Op.and]: [
                { user_email: userEmail },
                { service_id: serviceId }
            ]
        },
        transaction: t
    });

    return recurrentService;
};

const getUsersRecurrentServicesByEmail = async (userEmail, t = null) => {
    let recurrentServices = await RecurrentService.findAll(
        {
            where: {
                user_email: userEmail
            },
            transaction: t
    });

    return recurrentServices
};

const deleteRecurrentService = async (userEmail, serviceId, t = null) => {
    let recurrentService = await RecurrentService.findOne({
        where: {
            [Op.and]: [
                { user_email: userEmail },
                { service_id: serviceId }
            ]
        },
        transaction: t
    });

    if(!recurrentService) {
        return null;
    } else {
        await recurrentService.destroy({transaction: t});
        return recurrentService;
    }
};

module.exports = { addRecurrentService, getRecurrentServiceById, getUsersRecurrentServicesByEmail, deleteRecurrentService };