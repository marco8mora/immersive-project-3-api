//---------------ERRORS------------------

/**
 * @swagger
 * components:
 *   schemas:
 *     Error:
 *       type: object
 *       description: Error object with the error's name and message.
 *       properties:
 *         error:
 *           type: object
 *           description: The error's details
 *           properties:
 *             name:
 *               type: string
 *               description: The error's name
 *               example: NamedError
 *             message:
 *               type: string
 *               description: The error's message
 *               example: Error occurred processing the request
 *         data:
 *           type: object
 *           description: The empty data object.
 *           example: {}
 *     ErrorWithData:
 *       type: object
 *       description: Error object with the error's name and message, and a data object contains the data the prompted the error.
 *       properties:
 *         error:
 *           type: object
 *           description: The error's details
 *           properties:
 *             name:
 *               type: string
 *               description: The error's name
 *               example: NamedError
 *             message:
 *               type: string
 *               description: The error's message
 *               example: Error occurred processing the request
 *         data:
 *           type: object
 *           description: The data object that contains erroneous data.
 *           example: {email: example@example.com}
 */

//---------------MODELS------------------

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *           description: The user's email
 *           example: 
 *             $ref: '#/components/examples/emailExample'
 *         id:
 *           type: string
 *           description: The user's ID number
 *           example: 123456789
 *         full_name:
 *           type: string
 *           description: The user's full name
 *           example:
 *             $ref: '#/components/examples/fullNameExample'  
 *         photo_url:
 *           type: string
 *           description: URL to the user's profile picture
 *           example: https://innostudio.de/fileuploader/images/default-avatar.png
 *         source_income:
 *           type: string
 *           description: The user's main source of income
 *           enum: [employed_salaried, business_owner, self_employed, retired, investor, other]
 *           example: business_owner
 *     NewUser:
 *       allOf:
 *         - type: object
 *           properties:
 *             password:
 *               type: string
 *               description: The user's password
 *               example: 
 *                 $ref: '#/components/examples/passwordExample'
 *         - $ref: '#/components/schemas/User'
 *     Account:
 *       type: object
 *       properties:
 *         number:
 *           type: string
 *           description: The account's number
 *           example: 
 *             $ref: '#/components/examples/accountNumberExample'
 *         currency:
 *           type: string
 *           description: The account's currency
 *           enum: [crc, usd]
 *           example: crc
 *         balance:
 *           type: number
 *           description: The account's current balance
 *           example: 100.00
 *         user_email:
 *           type: string
 *           description: The email of the account's user
 *           example: 
 *             $ref: '#/components/examples/emailExample'
 *     NewService:
 *       type: object
 *       properties:
 *         category:
 *           type: string
 *           description: The service's main category
 *           enum: [public, education, insurance, taxes, donations, other]
 *           example: insurance
 *         subcategory:
 *           type: string
 *           description: The service's sub-category
 *           example: Water Services
 *         name:
 *           type: string
 *           description: The service's name
 *           example: Home Water Supply
 *         input_type:
 *           type: string
 *           description: The service's expected input type for their client ID's
 *           enum: [identification, phone_number, social_security_number, email]
 *           example: identification
 *     Service:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: string
 *               description: The service's ID
 *               example: 5
 *             account_number:
 *               type: string
 *               description: The account's number where the service received its payments
 *               example:
 *                 $ref: '#/components/examples/accountNumberExample'
 *         - $ref: '#/components/schemas/NewService'
 *     
 *     BaseTransaction:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The ID assigned to the transaction.
 *           example: 
 *             $ref: '#/components/examples/transactionIdExample'
 *         created_at:
 *           type: string
 *           description: The date and time of when the transaction was stored.
 *           example: 
 *             $ref: '#/components/examples/datetimeExample'
 *         amount:
 *           type: number
 *           description: The amount transfered in the destinations account currency.
 *           example: 250.05
 *         description:
 *           type: string
 *           description: A description of the transaction.
 *           example: Payment for services provided.
 *         account_credited:
 *           type: string
 *           description: Account where the funds are added to.
 *           example:
 *             $ref: '#/components/examples/accountNumberExample'
 *     InternalTransaction:
 *       allOf:
 *         - type: object
 *           properties:
 *             account_debited:
 *               type: string
 *               description: Account where the funds are substracted from, can be null if the money was added through Add Money.
 *               example: 
 *                 $ref: '#/components/examples/accountNumberExample'
 *         - $ref: '#/components/schemas/BaseTransaction'
 *     ExternalTransaction:
 *       allOf:
 *         - type: object
 *           properties:
 *             external_account:
 *               type: string
 *               description: External account where the funds are substracted from.
 *               example: 
 *                 $ref: '#/components/examples/accountNumberExample'
 *         - $ref: '#/components/schemas/BaseTransaction'
 *     ServiceTransaction:
 *       allOf:
 *         - type: object
 *           properties:
 *             is_service_payment:
 *               type: boolean
 *               description: Value that indicates wether the transaction was a service payment or not.
 *               example: true
 *         - $ref: '#/components/schemas/InternalTransaction'
 *     FullTransaction:
 *       allOf:
 *         - $ref: '#/components/schemas/InternalTransaction'
 *         - $ref: '#/components/schemas/ExternalTransaction'
 *         - $ref: '#/components/schemas/ServiceTransaction' 
 */