const ItemNotFoundError = (itemName, data = null) => {
    let error = new Error('Item not found in database', { cause: { item: itemName, data: data }});
    error.name = 'ItemNotFoundError';
    return error;
};

module.exports = ItemNotFoundError;