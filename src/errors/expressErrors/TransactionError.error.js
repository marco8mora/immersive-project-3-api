const TransactionError = (data) => {
    let error = new Error('Transaction could not be completed', { cause: { code: 409, data: data }})
    error.name = 'ClientError';
    return error;
};

module.exports = TransactionError;