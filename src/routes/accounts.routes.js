const express = require('express');
const authenticate = require('../middleware/authorize.middleware');
const { getAccountByNumber, getAccountsByUserEmail } = require('../services/account.service');
const ServerError = require('../errors/expressErrors/ServerError.error');
const NotFoundError = require('../errors/expressErrors/NotFoundError.error');

let accountsRouter = express.Router();
let successfulResponse = {
    data: {}
};

/**
 * @swagger
 * /accounts/user:
 *   get:
 *     summary: Retrieve an user's accounts.
 *     description: Retrieve the accounts associated to a user.
 *     responses:
 *       200:
 *         description: User's accounts retrieved successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   description: List of user's accounts
 *                   items:
 *                     $ref: '#/components/schemas/Account'
 *       404:
 *         $ref: '#/components/responses/404NotFoundError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'   
 */
accountsRouter.route('/user')
    .get(authenticate, async (req, res, next) => {
        try {
            const accounts = await getAccountsByUserEmail(req.jwt_payload.email);
            if(!accounts) {
                next(NotFoundError({account: req.email}))
            } else {
                successfulResponse.data = accounts.map(account => {
                    const { deleted_at, ...rest } = account.toJSON();
                    return rest;
                });
                res.json(successfulResponse);
            }
        } catch (error) {
            next(ServerError);
        }
        
    });

accountsRouter.param('number', (req, res, next, number) => {
    req.number = number;
    next();
});

/**
 * @swagger
 * /accounts/{number}:
 *   get:
 *     summary: Retrieve an account.
 *     description: Retrieve a single account, can be use to get an account's information.
 *     parameters:
 *       - in: path
 *         name: number
 *         required: true
 *         description: The number of the account.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Account retrieved successfully.
 *         content:
 *           application/json:
 *            schema:
 *              $ref: '#/components/schemas/Account'
 *       404:
 *         $ref: '#/components/responses/404NotFoundError'
 *       500:
 *         $ref: '#/components/responses/500ServerError'
 *       
 */
accountsRouter.route('/:number')
    .get(authenticate, async (req, res, next) => {
        try {
            const account = await getAccountByNumber(req.number);
            if(!account) {
                next(NotFoundError({account: req.number}))
            } else {
                const { deleted_at, ...rest } = account.toJSON();
                successfulResponse.data = rest;
                res.json(successfulResponse);
            }
        } catch {
            next(ServerError);
        }
    });

module.exports = accountsRouter;