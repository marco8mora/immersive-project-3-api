const DataFormatError = new Error('Format of data provided invalid or incomplete',
{cause: { code: 400}});
DataFormatError.name = 'DataFormatError';

module.exports = DataFormatError;