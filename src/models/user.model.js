const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');
const { hashPassword } = require('../helpers/hash.helper');

const userSourceIncome = [
    'employed_salaried',
    'business_owner',
    'self_employed',
    'retired',
    'investor',
    'other'
];

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        primaryKey: true,
        validate: {
            len: [1,254],
            isEmail: true
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        set(value) {
            this.setDataValue('password', hashPassword(value))
        }
    },
    id: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
            len: [9,9],
            isNumeric: true
        }
    },
    full_name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [1,100]
        }
    },
    photo_url: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [1, 250],
            isUrl: true
        }
    },
    source_income: {
        type: DataTypes.ENUM,
        values: userSourceIncome,
        allowNull: false,
        defaultValue: userSourceIncome[0],
        validate: {
            isIn: [userSourceIncome]
        }
    }
}, {
    sequelize,
    tableName: 'user',
    paranoid: true,
    createdAt: false,
    updatedAt: false,
    deletedAt: 'deleted_at'
});

module.exports = User;