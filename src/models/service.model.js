const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize.config');

const categories = [
    'public',
    'education',
    'insurance',
    'taxes',
    'donations',
    'other'
];

const input_types = [
    'identification',
    'phone_number',
    'social_security_number',
    'email'
];

const Service = sequelize.define('Service', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        validate: {
            isInt: true
        }
    },
    category: {
        type: DataTypes.ENUM,
        values: categories,
        allowNull: false,
        validate: {
            isIn: [categories]
        }
    },
    subcategory: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [1, 50]
        }
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
            len: [1,100]
        }
    },
    input_type: {
        type: DataTypes.ENUM,
        values: input_types,
        allowNull: false,
        validate: {
            isIn: [input_types]
        }
    }
}, {
    tableName: 'service',
    timestamps: false
});

module.exports = Service;