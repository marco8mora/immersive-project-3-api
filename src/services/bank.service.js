const sequelize = require('../config/sequelize.config');
const { Transaction } = require('sequelize');
const { addUser } = require('./user.service');
const { addService } = require('./service.service');
const { addAccount, getAccountByNumber } = require('./account.service');
const { addTransaction } = require('./transaction.service');
const { getServiceClientById } = require('./serviceClient.service');
const { exchangeCurrencies } = require('./exchange.service');
const { addRecurrentService } = require('./recurrentService.service');
const { generateIBANAccountNumber } = require('../helpers/rng.helper');
const ItemNotFoundError = require('../errors/businessErrors/ItemNotFoundError.error');
const InsufficientFundsError = require('../errors/businessErrors/InsufficientFundsError.error');
const ExchangeNotAvailableError = require('../errors/businessErrors/ExchangeNotAvailableError.error');

const addUserWithAccounts = async (userData) => {
    try {
        const result = await sequelize.transaction(async (t) => {
            const user = await addUser(userData, t);
    
            let colonesAccountNumber = generateIBANAccountNumber('CR');
    
            while (await getAccountByNumber(colonesAccountNumber, t) !== null) {
                colonesAccountNumber = generateIBANAccountNumber('CR');
            };
            
            const colonesAccountData = {
                number: colonesAccountNumber,
                currency: 'crc',
                balance: 0.0,
                user_email: user.email
            }
    
            const colonesAccount = await addAccount(colonesAccountData, t);
    
            let dollarsAccountNumber = generateIBANAccountNumber('US');
    
            while (await getAccountByNumber(dollarsAccountNumber, t) !== null) {
                dollarsAccountNumber = generateIBANAccountNumber('US');
            };
            
            const dollarsAccountData = {
                number: dollarsAccountNumber,
                currency: 'usd',
                balance: 0.0,
                user_email: user.email
            }
    
            const dollarsAccount = await addAccount(dollarsAccountData, t);
    
            let newUserWithAccounts = user.toJSON();
            newUserWithAccounts['colones_account'] = colonesAccount.toJSON();
            newUserWithAccounts['dollars_account'] = dollarsAccount.toJSON();
    
            return newUserWithAccounts;
        });

        return result;
    } catch(error) {
        throw error;
    }
};

const addServiceWithAccount = async (serviceData) => {
    try {
        const result = await sequelize.transaction(async (t) => {
            
            let colonesAccountNumber = generateIBANAccountNumber('CR');
    
            while (await getAccountByNumber(colonesAccountNumber, t) !== null) {
                colonesAccountNumber = generateIBANAccountNumber('CR');
            };
            
            const colonesAccountData = {
                number: colonesAccountNumber,
                currency: 'crc',
                balance: 0.0,
                user_email: null
            }
            
            const colonesAccount = await addAccount(colonesAccountData, t);
            
            serviceData['account_number'] = colonesAccount.number
            
            const service = await addService(serviceData, t);
    
            let newServiceWithAccount = service.toJSON();
    
            return newServiceWithAccount;
        });

        return result;
    } catch(error) {
        throw error;
    }
};

const addMoney = async (transactionData) => {
    try {
        const result = await sequelize.transaction(async (t) => {
            
            const accountCredited = await getAccountByNumber(transactionData.account_credited, t);

            await accountCredited.update( {
                balance: parseFloat((accountCredited.balance + transactionData.amount).toFixed(2))
            } , {transaction: t});

            const completedTransaction = await addTransaction(transactionData, t);
    
            return completedTransaction;
        });

        return result;
    } catch(error) {
        throw error
    }
};

const transferMoney = async (transactionData) => {
    try {
        const result = await sequelize.transaction({
            isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
        }, async (t) => {            
            const accountCredited = await getAccountByNumber(transactionData.account_credited, t);
            const accountDebited = await getAccountByNumber(transactionData.account_debited, t);

            // If the account credited does not exist
            if(!accountCredited) {
                throw ItemNotFoundError('account_credited', transactionData.account_credited);
            }

            // If the accounts have different currencies
            let amountInDebitedCurrency = transactionData.amount;
            if(accountDebited.currency !== accountCredited.currency) {
                amountInDebitedCurrency = await exchangeCurrencies(transactionData.amount,
                    accountCredited.currency, accountDebited.currency, t);

                if (!amountInDebitedCurrency) {
                    throw ExchangeNotAvailableError;
                }
            }

            // If the debited account doesn't have enough funds
            if((accountDebited.balance - amountInDebitedCurrency) < 0.0 ) {
                throw InsufficientFundsError;
            }

            // Add to credited account the amount.
            await accountCredited.update( {
                balance: parseFloat((accountCredited.balance + transactionData.amount).toFixed(2))
            } , {transaction: t});

            // Substract from debited account in its respective currency
            await accountDebited.update( {
                balance: parseFloat((accountDebited.balance - amountInDebitedCurrency).toFixed(2))
            } , {transaction: t});

            let completedTransaction = await addTransaction(transactionData, t)
    
            return completedTransaction;
        });

        return result;
    } catch(error) {
        throw error
    }   
};

const payService = async (paymentData) => {
    try {
        const result = await sequelize.transaction({
            isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
        }, async (t) => {
            // Get the bill associated with the client queried
            const billToPay = await getServiceClientById(paymentData.client_id, paymentData.service_id, t);

            // If the bill doesn't exists, that client doesn't owe anything to that service
            if(!billToPay) {
                throw ItemNotFoundError('bill_to_pay');
            }

            // Get the account to be debited
            const accountDebited = await getAccountByNumber(paymentData.account_debited, t);

            // Get the service and the account associated to the service
            const service = await billToPay.getService({transaction: t});
            const serviceAccount = await service.getAccount({transaction: t});

            // If the accounts have different currencies
            let amountInDebitedCurrency = billToPay.amount_owed;
            if(accountDebited.currency !== serviceAccount.currency) {
                amountInDebitedCurrency = await exchangeCurrencies(billToPay.amount_owed,
                    serviceAccount.currency, accountDebited.currency, t);

                if (!amountInDebitedCurrency) {
                    throw ExchangeNotAvailableError;
                }
                // Check if there are sufficient funds in the debited account's currency
                if((accountDebited.balance - amountInDebitedCurrency) < 0.0 ) {
                    throw InsufficientFundsError;
                }
            } else {
                // Both accounts have the same currency, just check for sufficient funds
                if((accountDebited.balance - amountInDebitedCurrency) < 0.0 ) {
                    throw InsufficientFundsError;
                }
            }
            
            // Substract from the debited account the amount in its respective currency
            await accountDebited.update( {
                balance: parseFloat((accountDebited.balance - amountInDebitedCurrency).toFixed(2))
            } , {transaction: t});

            // Add the owed_amount to the service account
            await serviceAccount.update( {
                balance: parseFloat((serviceAccount.balance + billToPay.amount_owed).toFixed(2))
            } , {transaction: t});

            // Create the transaction that represents this operation
            const completedTransaction = await addTransaction({
                amount: billToPay.amount_owed,
                description: `Payment of service ${service.name} for ${billToPay.client_id}`,
                is_service_payment: true,
                account_debited: accountDebited.number,
                account_credited: serviceAccount.number
            }, t);

            // Remove the paid bill
            billToPay.destroy({transaction: t});

            // If this payment was mark as a recurrent payment, add it
            if(paymentData.set_as_recurrent) {
                await addRecurrentService({
                    user_email: paymentData.email,
                    service_id: service.id,
                    date_paid: new Date().toISOString(),
                    client_id: paymentData.client_id
                }, t);
            }

            return completedTransaction;
        });

        return result;
    } catch(error) {
        throw error
    }
}

module.exports = { addUserWithAccounts, addServiceWithAccount, addMoney, transferMoney, payService }